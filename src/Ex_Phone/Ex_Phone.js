import React, { Component } from 'react'
import { data_phone } from './data_phone'
import Detail_Phone from './Detail_Phone'
import List_Phone from './List_Phone'

export default class Ex_Phone extends Component {
    state = {
        listPhone: data_phone, detailPhone: []
    }
    handleDetail = (newDetail) => {
        return this.setState({
            detailPhone: newDetail
        })
    }
    render() {
        return (
            <div className='row'>
                <div className="col-6"><List_Phone handleDetail={this.handleDetail} list={this.state.listPhone} /></div>
                <div className="col-6 "><Detail_Phone detail={this.state.detailPhone} /></div>
            </div>
        )
    }
}
