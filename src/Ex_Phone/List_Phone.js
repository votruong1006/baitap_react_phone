import React, { Component } from 'react'
import Item_Phone from './Item_Phone'

export default class List_Phone extends Component {
    render() {
        let list = this.props.list
        return (
            <div className='row'>
                {list.map((item) => {
                    return <Item_Phone handleDetail={this.props.handleDetail} phone={item} />
                })}
            </div>
        )
    }
}
